<?php
include ('header.php');
include ('sidebar.php');

?>
    <div id="content-wrapper">

    <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="<?php echo $custdir; ?>/acp/">Dashboard</a>
            </li>

        </ol>
        <div class="card mb-3">
            <div class="card-header">
                <i class="fad fa-plus-circle"></i> Add item</div>
            <div class="card-body">
                <?php
                if(isset($_POST['additem']))
                {
                    $itemid = stripslashes(mysqli_real_escape_string($mysqliA, $_POST['itemid']));
                    $itemname = stripslashes(mysqli_real_escape_string($mysqliA, $_POST['itemname']));
                    $itemcatval = stripslashes(mysqli_real_escape_string($mysqliA, $_POST['itemcat']));
                    $price = stripslashes(mysqli_real_escape_string($mysqliA, $_POST['price']));

                    if(empty($itemid && $itemname) && $price == '')
                    {
                        echo '
                            <div class="alert alert-warning" role="alert">
                              <i class="fad fa-exclamation-triangle"></i> Please check fields!
                            </div>
                         ';
                        header("refresh:3; url=$custdir/acp/item-add.php");
                    }
                    else
                    {
                        //let's check for duplicates
                        $item_check = $mysqliA->query("SELECT * FROM `store_items` WHERE `item_id` = '$itemid'") or die (mysqli_error($mysqliA));
                        $num_check = $item_check->num_rows;
                        if($num_check > 0)
                        {
                            echo '
                                <div class="alert alert-warning" role="alert">
                                  <i class="fad fa-exclamation-triangle"></i> This item already exists on store
                                </div>
                             ';
                            header("refresh:3; url=$custdir/acp/store-items.php");
                        }
                        else
                        {
                            //insert
                            $item_add = $mysqliA->query("INSERT INTO `store_items` (`item_id`, `item_name`, `price`, `category`) VALUES ('$itemid', '$itemname', '$price', '$itemcatval')") or die (mysqli_error($mysqliA));
                            if($item_add === true)
                            {
                                echo '
                                    <div class="alert alert-success" role="alert">
                                      <i class="fad fa-check-circle"></i> Item was added
                                    </div>
                                ';
                                header("refresh:3; url=$custdir/acp/store-items.php");
                            }
                        }
                    }

                }
                else
                {
                    ?>
                    <form name="additem" method="post">
                        <div class="form-group">
                            <label for="itemid">ItemID</label>
                            <input type="text" name="itemid" class="form-control" required="required">
                            <small>You can get item id from wowhead</small>
                        </div>
                        <div class="form-group">
                            <label for="itemname">Item Name</label>
                            <input type="text" name="itemname" class="form-control" required="required">
                            <small>Enter item name!</small>
                        </div>
                        <div class="form-group">
                                <label for="itemcat">Category</label>
                                <select name="itemcat" class="form-control">
                                    <?php
                                        $category_query = $mysqliA->query("SELECT * FROM `store_items_categorys`") or die (mysqli_error($mysqliA));
                                        $num_query = $category_query->num_rows;
                                        if($num_query < 1)
                                            {
                                            echo '<tr><td colspan="6">There are no Categorys!</td></tr>';
                                        }
                                        else
                                        {
                                            while($res = $category_query->fetch_assoc())
                                            {
                                                $categoryID = $res['id'];
                                                $categoryName = $res['name'];

                                                echo 
                                                '
                                                    <option value="'.$categoryID.'">'.$categoryName.'</option>
                                                ';
                                            }
                                        }

                                    ?>
                                </select>
                        </div>
                        <div class="form-group">
                            <label for="price">Item Price</label>
                            <input type="text" name="price" class="form-control" required="required">
                            <small>How many <span class="badge badge-warning"><i class="fad fa-coin"></i></span> (coins) this item will cost! </small>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success" name="additem"><i class="fad fa-plus-circle"></i> Add item</button>
                        </div>
                    </form>
                    <?php
                }

                ?>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
<?php
include ('footer.php');
?>
<?php include('header.php'); ?>

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-12">
                <!-- Blog Post -->
                <div class="card mb-4" id="card-wow">
                    <div class="card-body">
                        <h2 class="card-title"><i class="fad fa-unlock-alt"></i> Reset password</h2>
                        <p class="text-center">
                            <?php
                            if (isset($_POST['reset']))
                            {
                                //do insert
								$email = stripslashes(mysqli_real_escape_string($mysqliA, $_POST['email']));

                                //let's check login
                                $check_login = $mysqliA->query("SELECT * FROM `battlenet_accounts` WHERE `email` = '$email';") or die (mysqli_error($mysqliA));
                                $num_acc = $check_login->num_rows;
                                if($num_acc < 1)
                                {
                                    echo '
                                        <div class="alert alert-warning" role="alert">
                                            <i class="fad fa-exclamation-circle"></i> There is no user with this email address!
                                        </div>
                                    ';
                                    header("refresh:3; url=$custdir/recover.php");
                                }
                                else
                                {
									$allwoedchars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJLMNOPRQSTUVWXYZ01234567890";
									$password = substr(str_shuffle($allwoedchars), 0, 10);
                                    $bnet_pass = bin2hex(strrev(hex2bin(strtoupper(hash("sha256",strtoupper(hash("sha256", strtoupper($email)).":".strtoupper($password)))))));
									$bnet_upper_pass = strtoupper($bnet_pass);
									
									//update account passt
									$udate_acc_pass = $mysqliA->query("UPDATE `battlenet_accounts` SET `sha_pass_hash` = '$bnet_upper_pass' WHERE `email` = '$email';");
									
									$to = $email;

									// Subject
									$subject = 'Your new password';

									// Message
									$message = '
									<html>
									<head>
									  
									</head>
									<body>
									  <p>Your new password is: <strong> '. $password .'</strong></p>
									</body>
									</html>
									';

									// To send HTML mail, the Content-type header must be set
									$headers[] = 'MIME-Version: 1.0';
									$headers[] = 'Content-type: text/html; charset=iso-8859-1';

									// Additional headers
									$headers[] = 'To: ' . $email .' <' . $email .'> ';
									$headers[] = 'From: '. $site_name .' <'. $site_contact .'>';

									// Mail it
									mail($to, $subject, $message, implode("\r\n", $headers));
									echo '
									<div class="alert alert-success" role="alert">
									  <i class="fad fa-check"></i> Your passwrd has been reset!<br />
									  An email with your new password has been sent to <strong>' . $email . '</strong>. Please check SPAM/Junk also!
									</div>
									';
								
                                }
                            }
                            else
                            {
                            ?>
								<form name="reset" method="post" action="" enctype="multipart/form-data">
									<div class="form-group">
										<input type="email" class="form-control" name="email" placeholder="Your email address" required>
									</div>
									<button type="submit" name="reset" class="btn btn-warning form-control"><i class="fad fa-unlock-alt"></i> Reset password</button>
								</form>
								<br />
							<?php
							}
                         ?>
                        <br/>
                        </p>
                    </div>
                    <div class="card-footer text-muted">
                    </div>
                </div>
            </div>

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->

<?php include('footer.php'); ?>